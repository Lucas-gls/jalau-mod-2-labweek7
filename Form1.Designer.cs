﻿namespace LabWeek7
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            listView1 = new ListView();
            Exportar = new Button();
            Importar = new Button();
            AddItem = new Button();
            RemoveItem = new Button();
            button1 = new Button();
            OrderAZ = new Button();
            SuspendLayout();
            // 
            // listView1
            // 
            listView1.LabelEdit = true;
            listView1.Location = new Point(150, 50);
            listView1.Name = "listView1";
            listView1.Size = new Size(486, 229);
            listView1.TabIndex = 0;
            listView1.UseCompatibleStateImageBehavior = false;
            // 
            // Exportar
            // 
            Exportar.Location = new Point(150, 326);
            Exportar.Name = "Exportar";
            Exportar.Size = new Size(246, 23);
            Exportar.TabIndex = 1;
            Exportar.Text = "Exportar";
            Exportar.UseVisualStyleBackColor = true;
            Exportar.Click += Exportar_Click;
            // 
            // Importar
            // 
            Importar.Location = new Point(402, 326);
            Importar.Name = "Importar";
            Importar.Size = new Size(234, 23);
            Importar.TabIndex = 2;
            Importar.Text = "Importar";
            Importar.UseVisualStyleBackColor = true;
            Importar.Click += Importar_Click;
            // 
            // AddItem
            // 
            AddItem.Location = new Point(150, 285);
            AddItem.Name = "AddItem";
            AddItem.Size = new Size(246, 23);
            AddItem.TabIndex = 3;
            AddItem.Text = "Adicionar Item";
            AddItem.UseVisualStyleBackColor = true;
            AddItem.Click += AddItemButton;
            // 
            // RemoveItem
            // 
            RemoveItem.Location = new Point(402, 285);
            RemoveItem.Name = "RemoveItem";
            RemoveItem.Size = new Size(234, 23);
            RemoveItem.TabIndex = 4;
            RemoveItem.Text = "Remover Item";
            RemoveItem.UseVisualStyleBackColor = true;
            RemoveItem.Click += RemoveItemButton;
            // 
            // button1
            // 
            button1.Location = new Point(12, 415);
            button1.Name = "button1";
            button1.Size = new Size(75, 23);
            button1.TabIndex = 5;
            button1.Text = "Salvar erros";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // OrderAZ
            // 
            OrderAZ.Location = new Point(100, 50);
            OrderAZ.Name = "OrderAZ";
            OrderAZ.Size = new Size(44, 23);
            OrderAZ.TabIndex = 6;
            OrderAZ.Text = "A-Z";
            OrderAZ.UseVisualStyleBackColor = true;
            OrderAZ.Click += OrderAZ_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(OrderAZ);
            Controls.Add(button1);
            Controls.Add(RemoveItem);
            Controls.Add(AddItem);
            Controls.Add(Importar);
            Controls.Add(Exportar);
            Controls.Add(listView1);
            Name = "Form1";
            Text = "Form1";
            ResumeLayout(false);
        }

        #endregion

        private ListView listView1;
        private Button Exportar;
        private Button Importar;
        private Button AddItem;
        private Button RemoveItem;
        private Button button1;
        private Button OrderAZ;
    }
}
