
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace LabWeek7
{
    public partial class Form1 : Form
    {
        private List<Exception> errors;

        public Form1()
        {
            InitializeComponent();
            errors = new();
            InitializeView();
        }

        private void InitializeView()
        {
            listView1.View = View.Details;
            listView1.Columns.Add("Texts", -2, HorizontalAlignment.Left);
        }

        private void Importar_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Arquivos de texto (*.txt)|*.txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    listView1.Items.Clear();
                    using (StreamReader reader = new StreamReader(openFileDialog.FileName))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            listView1.Items.Add(new ListViewItem(line));
                        }
                    }
                }
                catch (Exception error)
                {
                    errors.Add(error);
                    MessageBox.Show("Erro ao importar arquivo.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Exportar_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Arquivos de texto (*.txt)|*.txt";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                    {
                        foreach (ListViewItem item in listView1.Items)
                        {
                            writer.WriteLine(item.Text);
                        }
                    }
                }
                catch (Exception error)
                {
                    errors.Add(error);
                    MessageBox.Show("Erro ao Exportar arquivo.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void AddItemButton(object sender, EventArgs e)
        {
            listView1.Items.Add("text to edit");
        }

        private void RemoveItemButton(object sender, EventArgs e)
        {
            listView1.View = View.Details;

            if (listView1.Items.Count > 0)
                listView1.Items.RemoveAt(listView1.Items.Count - 1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (errors.Count > 0)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog
                {
                    Filter = "Arquivos de texto (*.txt)|*.txt"
                };

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                    {
                        errors.ForEach(error => writer.WriteLine(error.Message));
                    }
                }
            }
            else
                MessageBox.Show("Voc� possui 0 erros", "Perfeito!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }

        private void OrderAZ_Click(object sender, EventArgs e)
        {
            //LINQ
            var itemsOrdered = listView1.Items.Cast<ListViewItem>()
                            .OrderBy(item => item.Text)
                            .ToArray();

            listView1.Items.Clear();

            if(itemsOrdered.Length > 0)
                foreach (var item in itemsOrdered)
                    listView1.Items.Add(item);
            else
                MessageBox.Show("Sua lista est� vazia", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
    }
}
